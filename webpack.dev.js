const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge({
    mode: 'development',
    devtool: 'source-map',
    devServer: {
        contentBase: './dev'
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ['style-loader']
            }
        ]
    }
}, common);
