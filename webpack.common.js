const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        main: ['./src/main.ts'],
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                enforce: 'pre',
                use: [
                    {
                        loader: 'tslint-loader',
                        options: { /* Loader options go here */ }
                    }
                ]
            },
            {
                test: /\.ts?$/,
                loaders: ['ts-loader'],
                exclude: '/node_modules/'
            },
            {
                test: /\.scss$/,
                use: ['css-loader', 'postcss-loader', 'sass-loader']
            },
            {
                test: /\.html$/,
                use: ['html-es6-template-loader']
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            name:'main',
            filename: 'index.html',
            template: './src/index.html',
        }),
    ],
    resolve: {
        extensions: ['.ts', '.js']
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            minChunks: 2,
            minSize: 1,
            name: 'common',
        }
    }
};
