interface ICharacter {
    name: string;
    age: number;
}

class Book {
    [Symbol.iterator]() {
        const story: string[] = this.storyText.split('\n');
        let currentIndex = 0;
        return {
            next: function () {
                if ( currentIndex === story.length - 1 ) {
                    return {value: story[currentIndex], done: true}
                } else if ( currentIndex < story.length - 1 ) {
                    const res = {value: story[currentIndex], done: false};
                    currentIndex++;
                    return res;
                }
            }
        }
    }

    private storyText: string = `there was a king ${this.character.name} and hi was ${this.character.age}, 
       and once he go to dark forest and ...
       you understand that here is no story ?
       and all of this just for do this task
   `;

    constructor(
        private character: ICharacter,
    ) {
    }

}

class Bookshelf {
    private currentBookIndex: number = 0;
    private storyTellerGenerator: IterableIterator<string>;

    constructor(
        private books: Book[] = []
    ) {
    }

    addBook(newBook: Book): void {
        this.books.push(newBook);
    };

    private storyReader = function* () {
        if ( this.currentBookIndex < this.books.length ) {
            for ( let i = 0; i < this.books.length; i++ ) yield [...this.books[this.currentBookIndex++]].join('');
        } else {
            this.currentBookIndex = prompt('all book is written, do you want start from begining ?') ?
                0 : this.currentBookIndex;
        }
    };

    readStory() {
        if ( this.storyTellerGenerator ) {
            return this.storyTellerGenerator.next();
        } else {
            this.storyTellerGenerator = this.storyReader();
            return this.storyTellerGenerator.next();
        }
    }
}

const newBook = new Book({name: 'Alex', age: 21} as ICharacter);
const newBook2 = new Book({name: 'Vlad', age: 23} as ICharacter);
const newBook3 = new Book({name: 'Dracula', age: 23} as ICharacter);
const bookShell = new Bookshelf([newBook, newBook2, newBook3]);
console.log(bookShell.readStory());
console.log(bookShell.readStory());
console.log(bookShell.readStory());

interface IHttpOptions {
     [key: string]: string,
}

interface IUser {
    name: string,
    height: string,
    mass: string,
    hair_color: string,
    eye_color: string;
    birth_year: string,
    gender: string,
    homeworld: string,
    films: string[],
    species: string[],
    vehicles: string[],
    starships: string[],
    created: string,
    edited: string,
    url: string
}

abstract class BaseHttp {

    abstract get: <T>(url: string, headers?: IHttpOptions) => Promise<T>;
    abstract remove: <T>(url: string, headers: IHttpOptions) => Promise<T>;
    abstract post: <T>(url: string, headers: IHttpOptions, body: T,) => Promise<T>;
    abstract put: <T>(url: string, headers: IHttpOptions, body: T,) => Promise<T>;

    makeRequest(method: string, url: string, options?: IHttpOptions): XMLHttpRequest {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        return this.createHeaders(options, xhr);
    }

    createHeaders(options: IHttpOptions, xmlHttpRequest: XMLHttpRequest): XMLHttpRequest {
        for ( let option in options ) {
            xmlHttpRequest.setRequestHeader(option, options[option])
        }
        return xmlHttpRequest;
    }

}

class PromisyfiedHttpClient extends BaseHttp {
    public get = <T>(url: string, headers?: IHttpOptions) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('GET', url, headers);
            xhr.onreadystatechange = () => {
                if ( xhr.readyState != 4 ) return;

                if ( xhr.status >= 200 && xhr.status < 400 ) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send();
        });
        return req
    };

    public post = <T>(url: string, headers?: IHttpOptions, body?: T) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('POST', url, headers);
            xhr.onreadystatechange = () => {
                if ( xhr.readyState != 4 ) return;

                if ( xhr.status >= 200 && xhr.status < 400 ) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send(JSON.stringify(body));
        });
        return req
    };

    public put = <T>(url: string, headers?: IHttpOptions, body?: T) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('PUT', url, headers);
            xhr.onreadystatechange = () => {
                if ( xhr.readyState != 4 ) return;

                if ( xhr.status >= 200 && xhr.status < 400 ) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send(JSON.stringify(body))
        });
        return req
    };

    public remove = <T>(url: string, headers?: IHttpOptions) => {
        const req: Promise<T> = new Promise((resolve, reject) => {
            const xhr = this.makeRequest('DELETE', url, headers);
            xhr.onreadystatechange = () => {
                if ( xhr.readyState != 4 ) return;

                if ( xhr.status >= 200 && xhr.status < 400 ) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(JSON.parse(xhr.response));
                }
            };
            xhr.send();
        });
        return req
    }
}

const http = new PromisyfiedHttpClient();
const url = 'https://swapi.co/api/people/';

http.get<IUser>(url+'1').then(res => console.log(res.eye_color, res.hair_color));

const promises: Promise<IUser>[] = [];
const urls: string[] = [];
for(let i = 1; i < 10; i++) {
    promises.push(http.get<IUser>(url+i));
    urls.push(url+1);
}

Promise.all(promises).then(resArr => resArr.forEach((hero: IUser) => console.log(hero.name)));
// @ts-ignore
Promise.all(urls.map(http.get)).then((items) => console.log(items));
